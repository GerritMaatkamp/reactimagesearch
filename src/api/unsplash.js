import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
         Authorization: 'Client-ID 7bc4a46c99b89d0c4893cfb3546b346b1456041cf36ad7fd2d8e32a490768124'
    }
});